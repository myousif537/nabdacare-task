import { Component, OnInit, OnDestroy, ChangeDetectorRef } from "@angular/core";
import { MediaMatcher } from "@angular/cdk/layout";
import { HelperService } from '../../services/helper.service';

@Component({
  selector: "app-location-map",
  templateUrl: "./location-map.component.html",
  styleUrls: ["./location-map.component.css"]
})
export class LocationMapComponent implements OnInit, OnDestroy {
  mobileQuery: MediaQueryList;

  lat = 30.044482654785146;
  lng = 31.235799312591553;


  locations : [] = [];



  private _mobileQueryListener: () => void;

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher, private helperServ : HelperService) {
    this.mobileQuery = media.matchMedia("(max-width: 600px)");
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);

    this.fillLocations();
  }

  ngOnInit() {}

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  fillLocations(){
    this.helperServ.getLocations().subscribe(locationsList=>{
      this.locations = locationsList.places
      console.log(this.locations)
    })
  }


}
