import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {LocationMapComponent} from './index'

const routes: Routes = [

  {
    path : 'map',
    component : LocationMapComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LocationRoutingModule { }

export const routedComponents=[LocationMapComponent]
