import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';

import { MaterialModule } from "../../material.module";
import {
  LocationRoutingModule,
  routedComponents
} from "./location-routing.module";
import { HttpClientModule } from "@angular/common/http";
import { AgmCoreModule } from "@agm/core";

import { HelperService } from "./index";

const services = [HelperService];


@NgModule({
  declarations: [routedComponents],
  imports: [
    MaterialModule,
    LocationRoutingModule,
    HttpClientModule,
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyBZR7Z7SyDkBaYL6IJZ-4FJQnNaobV6PAk"
    }),
    CommonModule
  ],
  providers: [...services]
})
export class LocationModule {}
