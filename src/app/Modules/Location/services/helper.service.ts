import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map, timeout } from "rxjs/operators";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: "root"
})
export class HelperService {
  constructor(private httpClient: HttpClient) {}

  public getLocations(): Observable<any> {
   return this.httpClient
      .request(
        "get",
        "https://admin.balsamee.com/api/v5/places?latitude=30.044482654785146&longitude=31.235799312591553&type=clinics&lang=en"
      )
      .pipe(
        timeout(6000),
        map(res => {
         if(res) return res;
        })
      );
  }


}
