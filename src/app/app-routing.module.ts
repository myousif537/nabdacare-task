import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';



const routes: Routes = [

  {
    path : '',
    redirectTo:'/location/map',
    pathMatch : 'full'
  },

  {
    path : 'location',
    loadChildren : ()=>import ('./Modules/Location/location.module').then(l=>l.LocationModule)
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
